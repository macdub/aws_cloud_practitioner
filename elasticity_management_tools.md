# MODULE-5: Elasticity and Management Tools
- Elastic Load Balancing (ELB)
  - performs the function over the scale set
  - distribute traffice
- Auto Scaling
  - out and in scaling over EC2 instances
  - alerts the ELB to adjust load balancing when scale changes
  - ensures proper number of instances needed
- Amazon CloudWatch
  - collects and aggregates metrics from the load balancer and instances
  - reports to Auto Scaling to trigger the scaling
  - generate alarms

## Elastic Load Balancer
- over entire region
- distributes traffic across multiple EC2 instances in multi-AZs
- register instance as targets in a target group, route traffic to a target group
- load balancer routes request at the application layer HTTP/HTTPS (L7)
- content and non-content aware setups

## Auto Scaling
- scale your EC2 capacity
- launch config is a template that is used for scaling
- create scaling policy that uses CloudWatch
- use alarms to monitor
- time base scaling

## CloudWatch
- monitoring service for resources and the apps you run
- visibility into resource utilization, op performance, overall demand patterns
- custom app specific metrics
- accessible via Management Console
- statistics consumer (splunk, nagios, etc.)
- raise alarms (email, auto scaling, etc.)

## Trusted Advisor
- provide guidance to help reduce cost, increase performance and improve security
- "Well Architected Framework" AWS whitepaper
  - [AWS Well Architected Home|https://aws.amazon.com/architecture/well-architected/]
  - [White Paper Home - Well-Architected Framework|https://aws.amazon.com/whitepapers/?whitepapers-main.sort-by=item.additionalFields.sortDate&whitepapers-main.sort-order=desc&awsf.whitepapers-content-category=content-category%23well-arch-framework]
