# MODULE-3: AWS Security, Identity, and Access Management

## AWS Shared Responsibility Model
- customers are responsible for security _IN_ the cloud
  - basically all the stuff that the customer manages. OS, Network, firewalls, applications, etc.
  - Think everything above the hyper-visor
- AWS is responsible for the security _OF_ the cloud
  - infrastructure / hardware
    - AZs, Regions, Edge Locations
  - Foundation Services
    - compute, storage, database, networking
  - Think everything below the hyper-visor

### Physical Security
- 24/7 trained security staff
- data centers in nondescript and undisclosed facilities
- two-factor auth for authorized staff
- authorization for data center access

### Assurance Programs
- Global
  - AICPA-SOC
  - ISO 9001, 279001, 27017, 27018
- US
- Europe
- Asia
- GDPR supported 100% for AWS services out of the box

- SSL Endpoints
  - use secure endpoints to establish secure communication sessions (HTTPS)
- Security Groups
  - Use security groups to configure firewall rules for instances
- VPC
  - use public and private subnets, NAT, and VPN support in your virtual private cloud to create low-level networking constraints for resource access

## Identity and Access Management (IAM)
1. manage AWS IAM users and access
2. manage AWS IAM roles and permissions
3. manage federated users and permissions

- IAM can really only lock down API calls
  - e.g. IAM can't regulate ssh
  - important but not the only solution; use in conjunction with other measures

### Policies
- JSON formats

### Roles
- circumvent the need for credentials
- allows for service account style access without a static user
- temporary credentials that expire

## IAM Authentication
- AWS Management Console
  - username / password
- AWS CLI or SDK SPI
  - Access Key and Secret Key
- Policies
  - JSON docs to describe permissions
- Roles
  - no associated credentials
  - users, applications, and servcies may assume IAM roles
  - uses a policy

## Use cases Temporary Security Credentials (AWS STS)
- cross-account access
- federation
- mobile users
- key rotation for EC2-based apps

1. access key id            -\
2. secret access key         |-temporary credentials
3. session token            -/
4. expiration               - 15 mintes to 36 hours

## Best practices
- delete aws account root access keys
- create individual IAM users
- use groups to assign perms to IAM users
- remove unnecessary users and credentials
- policy conditions for extra sec
- monitor activity in your AWS account
- rotate creds

## AWS CloudTrail
- records api calls for account
- deliver log files with info to s3 buck
- makes calls using AWS management consol, aws sdks, aws cli and hier-level services

## Quiz
1. Your web tier EC2 instances are unable to communicate with your database server instances. What is the first thing you should check?
  - KMS key policies
  - security group rules \[x\]
  - network ACL rules
  - AWS IAM roles

2. Which provides an additional layer of security for login via the AWS management console?
  - secret key
  - secret Access key
  - public/private key pair
  - multi-factor authentication \[x\]

3. You arel ogged in as an IAM user on the AWS management console. But you keep getting an error when trying to access any S3 buckets. What is the likely cause?
  - You are not part of an IAM group
  - you do not have MFA enabled
  - you need to log in as the root account user to access S3
  - there is no IAM access policy attached to your IAM user \[x\]

4. Which is a JSON document that provides a formal statement permissions in AWS?
  - inline role
  - policy \[x\]
  - group
  - role

5. your application has an EC2 instance which needs r/w acess to a DynamoDB table how would you implement this access?
  - use KMS key
  - create an IAM role that the EC2 instance can assume
  - use an IAM group \[x\]
  - add credentailas for an IAM user in a cred. file in the EC2 instance

6. Under the shared responsibilty model, which does AWS _not_ assume responsibilty?
  - Access to and encryption of customer data \[x\]
  - physical network infra
  - hypervisors
  - physical security of data centers
