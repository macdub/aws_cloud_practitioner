# Storage Services: Amazon S3 and Amazon EBS

## Elastic Block Storage (EBS)
- peristent block level storage volumes offter consistent an dlow-latency performance
- stored data is automatically replicated within its AZ
- snapshots are stored durably in S3
  - expand scope across multiple AZs of a region

- virtual disk drive lives in the cloud; could be SSD or HDD
- Given volume can only be connected to one VM at a time
- [All Things Distributed|https://allthingsdistributed.com]

### Lifecycle
0. Unused Space
1. Create
  - Call CreateVolume 1GiB to 16 TiB
2. ATtach
  - Call ATtachVolumne to affiliate with one EC2 Instance
3. Attached and In Use
  - format from EC2 instance OS
  - mount formatted rive
4. CreateSnapshot
  - snapshot to S3
5. Detatch
5. Delete
  - "Call DeleteVolume"

- SSD Backed volumne:
  - optimized for transactional workloads that involve frequent r/w ops with small I/O size
  - dominant in IOPS perf

- HDD backed volumes:
  - optimized for large streaming workloads
  - dominant in throughput (MiB/s)

- EBS is recommended when data must be quickly accessible and requires long-term persistence
- you can launch your EBS volumes as _encrypted_

- detaching a volumn via the API will automatically unmount from the host

### Use cases
- OS
- Databases
- Enterprise Applications
- Business cont
- Applications

## Simple Storage Service (S3)
- storage for the internet
- is an object store
- natively online, http access
- storage that allows you to store and retrieve any amount of data, any time, from anywhere on the web
- highly scalable, reliable, fast and durable
- two operations for object store
  - overwrite
  - delete
- designed for throughput
- 1/5th cost of EBS

### Use cases
- storage and backup
- application file hosting
- media hosting
- software deliver
- store AMIs and snapshots
- large static file storage

### Concepts
- stores data as objects within blocks
- object is composed of a file and optionally any metadata that describes that file
- up to 100 buckets in each account
- control access to the buck and its object
- object key is the unique identifier for an object in a bucket
  - ```
    http://doc.s3.amazonaws.com/2006-03-01/AmazonS3.html
           ^-^                  ^----------------------^
         bucket                  Object/Key
    ```
  - buckets are unique over the entire AWS bucket universe

### Features
- buckets
  - properties
    - versioning
    - access logging
    - static website hosting
    - object-level logging
    - default encryption
      - can specify kind of encryption (AES-256 (SSE-S3), AWS-KMS (SSE-KMS))
      - no auditing with SSE-S3; no extra cost
      - auditing with SSE-KMS; extra cost

  - management
    - lifecycle
      - can set rules to move data to colder storage
      - Glacier archival storage
      - colder the storage the cheaper it is per TiB

### Security
- you can controll access to buckest with objects
  - access control lists (ACLs)
  - bucket policies
  - identity

### Facts
- unlimited objects per bucket
- objects up to 5TB; no bucket size limit
- designed for 99.999999999% durability and 99.99% availability of objects over a given year
  - durability: may not be able to get to data, but still there
  - availability: can get to data
- can use HTTP/S endpoints to store and retrieve andy amount of data, at any time, from anywhere on the web
- highly scalable, reliable, fast, and inexpensive
- can use optional server-side encrytipon using AWS or customer-managed provided client-sid encryption
- auditing is provided by access logs
- provides standards-based REST and SOAP interfaces

### Pricing
- pay only for what you use
- no minimum fee
- prices based on location of your S3 bucket
- estimate monthly bill using smiipe monthly calc
- pricing is available as:
  - storage pricing
  - request pricing
  - data transfer pricing: data transferred out of S3

## Glacier
- long term low-cost archiing service
- optimal infrequently accessed data
- designed for 99.999999999% durability
- 3-5 hours standard access
- roughly $1/TiB

## Quiz
1. For cost optimization, which usage factors affect what you pay for S3?
  - The number of S3 buckets you create and use
  - Whether you turn on default encryption (SSE-S3)
  - Total size in GB of all objects being stored \[x\]
  - Storage class selected for the objects \[x\]

2. What would you do to take a backup of an EBS volume?
  - STore the volume in an RDS database
  - Store the volume in an S3 Bucket
  - Create an EBS snapshot \[x\]
  - store the volume in a DynamoDB table

3. Which best describes EBS?
  - managed database service
  - virtual hard disk in the cloud \[x\]
  - managed nosql database service
  - storage area network (SAN) in the cloud

4. You have a need to distribute a software archive to thousands of other developers across the world. Which AWS service would best meet the need?
  - EBS
  - S3 \[x\]
  - RDS
  - EFS

5. Which application is Glacier designed for? (pick 2)
  - infrequently accessed data \[x\]
  - data archives \[x\]
  - cached data
  - online transaction processing (OLTP)

6. Which feature can you use to keep logs in an S3 bucket for a specific period of time, and then delete automatically?
  - Lifecycle policy for an S3 bucket \[x\]
  - bucket policy
  - cross-origin resource sharing (CORS)
  - IAM policy
