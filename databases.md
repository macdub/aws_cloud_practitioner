# MODULE-4: AWS Databases

## Managed Database Services
- Amazon DynamoDB
  - low-latency NoSQL (millisecond)
  - no capacity limits
- Amazon ElastiCache
  - in-memory database
  - extremely low latency for read (microsecond)
- Amazon RDS
  - can bring own liscense
  - MariaDB, Oracle, SqlServer, Postgres, etc.
  - many kinds available
- Amazon Aurora
  - cloud native RDS
  - high performance
  - cross region replication with less than one second of lag
  - MySQL, Postgres only supported
- Amazon Neptune
  - graph database
- Amazon Redshift
  - data warehouse
- MongoDB aaS : general availability
- know the elevator pitch on these

|             | sql      | nosql                                |
|:-----------:|:--------:|:------------------------------------:|
| data store  | row/cols | key-value                            |
| schemas     | fixed    | dynamic                              |
| querying    | SQL      | focused on a collection of documents |
| scalability | vertical | horizontal                           |

## RDS
### Engine Types
- MySQL
- MariaDB
- SQL Server
- Oracle
- Postgres
- Aurora

### HA with multi-AZ
- synchronous replication across AZs
- DNS failover strategies

### Scaling
- instance type
  - change with minimum downtime
- storage capacity
  - existing instances
- read workloads could utilize read replicas
  - open source compatible
  - can be promoted to primary should it be needed

### Best Practices
- use VPC
- use encryption
- IAM policies
- security groups
- use SSL with DB instances
- use network encryptions

## DynamoDB
- allows to store any amount of data with no limit
- provides fast, predictable performance using SSDs
- allows you to easily provision and change the request capacity needed for each table
- is fully managed, NoSQL database service
- accommodate changing workloads with on-demand mode
- table is implemented as multiple servers hosting a single table "server-less"
- massive tables with large number of rows with very fast access times
- scale out unlimited
- Simple lookups
- low cost

### Primary Key
- partition key is the primary key
- partition key is expected to be unique or near unique
- need partition key
- may need sort key

### Provision Throughput
- specify how much provisioned throughput capacity you need for r/w
- allocates the necessary machine resources to meet your needs

- DynamoDB is more key-value lookups
- MongoDB the complex JSON document store

### Supported Operations
- Query
  - query a table using the parition key and optional sort key filter
  - table has secondary index, query using its key- most efficient way to retrive itmes from table or secondary index
- Scan
  - can a table or secondayr index
  - reads every item

### High Availability
- is HA by default as it isn't a specific server setup

## Quiz
1. Your company wants to migrate an application to AWS. The application has a .NET layer connecting to a MySQL database. The goal is to move to a cloud-native solution. Which database service in AWS would be the ideal choice?
  - DynamoDB
  - Aurora \[x\]
  - RDS
  - Athena

2. Select TWO AWS services that offer serverless technology
  - S3 \[x\]
  - EMR
  - EC2
  - DynamoDB \[x\]

3. Which is the managed data warehousing service in AWS?
  - Snowball
  - RedShift \[x\]
  - Simple Storage Service
  - Amazon EMR

4. Which database engine is NOT supported by RDS?
  - Oracle
  - MS Sql Server
  - Postgres SQL
  - DB2 \[x\]

5. For RDS MySQL, how would you implement a disaster recovery solution across regions with the lowest possible RPO?
  - Configure a remote read replica \[x\]
  - create snapshots periodically and copy over
  - use S3 cross-region replication
  - take advantage of the RDS Multi-AZ feature

6. Your tow-tier application in the cloud uses EC2 instances and an RDS database. It has  become extremely ppulatr, and to scal you need a chache with microsecond-latency read access. Which AWS service can help?
  - DynamoDB
  - Kinesis
  - ElastiCache \[x\]
  - RDS with the Multi-AZ feature
