# MODULE-2 AWS Foundational Services
- Elastic Compute Cloud (EC2)
- Virtual Private Cloud (VPC)
- Storage Services
  - Elastic Block Store (EBS)
  - Simple Storage Service (S3)


## Elastic Compute Cloud (EC2)
- Resizeable compute capacity
- Complete control of compue resources
- Reduce time required to obtain and boot new server instances
- _scale capacity_ as need changes
- pay only for capacity used
- Linux or Windows
- Deploy across AWS _regions_ and _AZs_ for reliability
- Use tags to help manage your EC2 resources

- save money by deploying windows on EC2? :: Eh what? Is this over linux deployments or versus other cloud providers?
  - can bring in own windows license

- [EC2 Pricing Page|https://aws.amazon.com/ec2/pricing]
  - Linux machines are charged by the second; prices are listed by the hour
- Has both Intel and AMD CPUs available; ARM is also available
- GPU instances also available
- Storage optimized options for large IOPs

## Launching an Instance via Management Console
- determine Region to use
- launch from a pre-config Amazon Machine Image (AMI)
  - market place images available
  - can use self built images
- choose instance type based on CPU, memory, storgage, and network requirements
- configure network, IP address, security group, storage volumne, tags, and key pair

- compute and storage are loosely coupled
  - can resize compute and still points to the correct storage
- can make EBS (storage block) snapshots that are triplicated across the region

- web wizard is a front end to RESTful calls to start instances
- self built images look easily usable to duplicate
- can set processor generation (m5, m4, ...)
  - m5 : Intel
  - m5a : AMD
  - Older generation chips tend to cost more versus the newer generation
    - migration is relatively painless; stop the instance, change family, and restart; limited down time on this
    - ? any sort of rolling update on active instances

- Instance configuration
  - Advanced Details lets you execute a script at start in the _user data_ input box
  - e.g.
    #! /bin/sh
    yum -y install httpd
    chkconfig httpd on
    /etc/init.d

- Must provide a key pair or create a new key pair as these machines are public facing and the key pair is used for access

- newer generation instance types usually have better price-to-performance ratio

### Purchase Options
1. On-Demand Instances
  - pay by the hour

2. Reserved Instances
  - Purchase, at a significant discount, instances that are always available
  - 1-year to 3-year terms

3. Scheduled Instances
  - Purchase instances that are always availabe on the specified recurring schedule, for a 1-year term

4. Spot Instances
  - Bid on unused instaces, which can run as long as they are available and your bid is above the Spot Price
  - Spot Price is typically lower than the On-Demand price
    - As demand grows the Spot Prices increases and can go past the On-Demand price
    - Once bid is less than the Spot Price then Amazon will likely take those instance back from your use

5. Dedicated Instances
  - Pay, by the hour, for instances that run on single-tenant hardware
  - Can bring MS datacenter liscense to the cloud that could be more favorable versus other cloud providers

6. Dedicated Hosts
  - Pay for a physical host that is fully dedicated to running your instances

## Networking: VPC
- provision a private, isolated virtual network
- complete control over your virtual netwroking environment
- subnet work is a higher level than working at the switch level

- subnet defines a range of IP addresses in your VPC
- lauch AWS resources into a subnet that you select
- private subnet should be used for resources that wont be accessible over the internet
- public subnet should be used for resources that will be accessed over the internet
- VPN only subnet

### Security in VPC
- Security Groups
- Network access control lists (ACLs)
- Key Pairs
- layers of security
  - web app firewall (in front of internet gateway)
  - network ACLs (in front of subnet)
  - cloud level firewall (in front of instance / security group)

### Hybrid Cloud
- Extending On-Prem Network to AWS via VPN connection
- Virtual Private Gateway (VGW) (advanced architecture class)
  - low cost; don't charge for VPN connection; egress charge only
- Use a private link from On-Prem to AWS
  - AWS Direct Connect
  - Co-located facility
  - more throughput and better latency; more costly, but less than other cloud providers
  - Could be as fast as 40Gbps over this link

## Quiz
1. Which AWS service lets you run code without having to worrya bout provisioning any underlying resources (VMs, storage, or network)?
  - EC2
  - DynamoDB
  - Elastic Load Balancer
  - Lambda \[x\]

2. Which EC2 optino is best for long-term workload with a predictable usage pattern?
  - Dedicated Host
  - Reserved Instances \[x\]
  - On-Demand Instances
  - Spot Instances

3. You need to prototype a new feature in your software application using development servers that you will need for the next five weeks. Which type of instance should you porvision?
  - Spot instances
  - On-Demand instances \[x\]
  - Reserved instances
  - partial up front reserved instances

4. Which AWS networking service enables your enterprise to create a virtual netowrk with in AWS?
  - API gateway
  - Route 53
  - Virtual Private Cloud (VPC) \[x\]
  - Direct connect

5. Which AWS offering enables customers to find, buy and immediately start using software solutions in their AWS environment?
  - AWS Config
  - AWS OpsWorks
  - AWS SDK
  - AWS Marketplace \[x\]

6. Your project requires 90 hours of compute time. There is no hard deadline, and work can be stoopped and restared without adverse effect. Which computing option offers the most cost-effective solution?
  - Scheduled reserved instances
  - Spot instances \[x\]
  - Reserved instances
  - On-Demand instances

## AWS Cloud Practitioner Essentials (Second edition)
- [https://www.aws.training/learningobject/curriculum?id=27076]
- free / 6 hours / always available
