# MODULE-1: Introduction and History of AWS
- quizizz.com :: quiz tool

## History
- '94 incorportated
- '95 bookstore
- '05 amazon publishing
- '06 AWS launch
- '07 Kindle launch
- '11 Amazon Fresh launch
- '12 Game Studio
- '13 Amazon Art
- '14 PrimeNow
- '15 Amazon Home Services / Amazon Echo

### AWS
- Enable business and devs to use web services to build scalable, sohpisitcated applictions
- Offers over 129 fully featured services for compute, storage, databases, netowrking, analytics, machine learning and AI, IoT, mobile, security, hybrid, app dev, deployment, and management.
- Lauched total of 311 new features/services year to date (as of 20180331) for a total of 4653 new features/services

- Startups: from the spark to first customer; IPO and beyond.
- Public Sector: paving the way for innovation and support workd-changing projects in gov, edu, and non-profit orgs
- Enterprise: mature set of services specifically designed for the unique security, compliance and priv.

#### Core benefits
- trade capital expense for variable expense
- benefit from massive economies of scale
  - 60+ price reductions in the last decade and continuing
- stop guessing capacity
- increase speed and agility
- stop spending money on running an maintaining data centers
- go global in minutes

- positioned as a leader in the Gertner Magic quadrant for cloud infra as a service
  - rated highest in execution and furthest in vision with in the Leaders Quadrant

## Foundation Services
| Compute                    | Networ  k              | Storage         | Security & Identity | Application   |
|:--------------------------:|:----------------------:|:---------------:|:-------------------:|:-------------:|
| EC2                        | CloudFront             | EFS             | Inspector           | WorkDocs      |
| Elastic Container Registry | Route 53               | S3 Glacier      | Artifact            | WorkMail      |
| Elastic Container Service  | VPC                    | S3              | Certificate Manager | AppStream 2.0 |
| Ligthsail                  | Direct Connect         | Snowball        | CloudHSM            | WorkSpaces    |
| Batch                      | Elastic Load Balancing | Storage Gateway | Directory Service   |               |
| Elastic Beanstalk          |                        |                 | IAM                 |               |
| Lambda                     |                        |                 | KMS                 |               |
|                            |                        |                 | Organizations       |               |
|                            |                        |                 | Shield              |               |
|                            |                        |                 | WAF                 |               |

## Platform Services
1. Databases
  - DynamoDB
  - ElastiCache
  - RDS
  - RedShift

2. Analytics
  - Athena
  - CloudSearch
  - EMR
    - Hadoop aaS
  - Elasticsearch Service
  - Kinesis
  - QuickSight

3. Application Services
  - API Gateway
  - AppStream 2.0
  - Elastic Transcoder
  - SWF
  - Step Functions

4. Management Tools
  - CloudWatch
  - CloudFormation
  - CloudTrail
  - Config
  - Managed Services
  - OpsWork

- didn't get it all; can pull from the AWS page

## Global Infrastructure
- Regions
  - 22 regions
    - ~69 availabilty zones : changes continuously

  - Geographic locations
  - two points in region have no more than 2ms latency between the two
    - synchronus replication
  - fiber channel distance
  - Consist of at least two availabilty zones
  - [Map of AWS physical|https://infrastructure.aws]

- Availabilty Zones
  - exist inside of regions
  - Clusters of data centers
  - Isolated from failures in other availability zones

- replication across regions runs over AWS backbone; async replication
- provides governance; data localization, etc.

- 2 ISPs per DC
- 2 power supplies per DC

- 100+ edge locations
- local points of presence that support AWS services:
  - Amazon Route 53
  - Amazon CloudFront
    - implement CDN
  - AWS WAF
  - AWS Shield

### Quiz
1. What best describes elasticity
  - Ability of a system to be accessible when you attempt to access it
  - Ability of  asystem to grow in size, capacity or scope
  - Ability of a system to withstand one or more component failures and still remain functional
  - Ability of  a system to grow or shrink based on demand \[x\]

2. Which best describes an AWS region?
  - A collection of databaess that can only be accessed from one geo region
  - A collection of data centers are spread eveny around a continent
  - A distinct location within a geo area designed to provided HA to that area \[x\]
  - A console providing a quick global view of your cloud computing environment

3. Which best describes Availability Zones?
  - Distinct locations with in an AWS region that are inter-connected, yet isolated with respect to failures \[x\]
  - Two zones containing compute resources designed to automatically maintain sync copies of each other's data
  - Content Delivery Network used to accelerate web sites
  - Restricted areas required for use in creating VPCs

4. Which is correct?
  - Number of regions > Num availability zones > num edge locs
  - num avail. zones > num edge locs < num regions
  - num edg locs > num avail zones > num regions \[x\]
  - num avail zones > num regions > num edge locations

5. Where is CloudFront content cached?
  - Edge location \[x\]
  - Availability Zone
  - Region
  - ONe of your VPCs

6. Which statement is true? pick 2
  - All AWS data centers within a country are grouped into a single AZ
  - A region consists of two or more AZs \[x\]
  - Multiple edge locations comprise an AZ
  - AZ within a region are no more than tens of miles apart \[x\]


## MSCI AWS Account Team
- Jim Giovinazzo
  - Enterprize Account Manager
  - jimgio@amazon.com
  - 917-952-3940

- Muhammad Mansoor
  - Enterprise Solutions ARchitet
  - muhmanso@amazon.com
  - 201-401-9223
